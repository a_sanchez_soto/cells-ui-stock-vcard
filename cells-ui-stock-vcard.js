import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-stock-vcard-styles.js';
import '@bbva-web-components/bbva-form-checkbox';
import '@bbva-web-components/bbva-button-default';
import '@vcard-components/cells-util-behavior-vcard';
import '@vcard-components/cells-theme-vcard';
import '@cells-components/coronita-icons';
import '@cells-components/cells-icon';
import '@vcard-components/cells-ui-message-alert-vcard';
import '@vcard-components/cells-ui-autocomplete-vcard';
/**
This component ...

Example:

```html
<cells-ui-stock-vcard></cells-ui-stock-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiStockVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-ui-stock-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      items: {
        type: Array
      },
      itemsDisplay: {
        type: Array
      },
      oficinas: {
        type: Array
      },
      oficinaFilter: {
        type: Object
      }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.items = [];
    this.itemsDisplay = [];
    this.oficinas = [];
    this.oficinaFilter = null;
    this.updateComplete.then(() => {
      this.getById('input-search').addEventListener('input', () => {
        this.filterItems();
        this.requestUpdate();
      });
    });

    this.addEventListener('clear-value-autocomplete', async (event)=>{
      console.log('clear-value-autocomplete',event.detail);
      if(event.detail.name === 'autocomplete-oficina-stock'){
        this.oficinaFilter = null;
        await this.requestUpdate();
        this.filterItems();
        this.requestUpdate();
      }
    });

  }

  filterItems(onlyOficina) {
    let query = this.getById('input-search').value;
        if (query || onlyOficina) {
          query = query.toUpperCase();
          this.itemsDisplay = this.items.filter((item) => {
            let fOficina = true;
            if(this.oficinaFilter && item.oficina.codigo !== this.oficinaFilter.codigo) {
              fOficina = false;
            }
            if(onlyOficina && this.isEmpty(query)){
              return fOficina;
            }
            return ((item.tarjeta.bin + item.tarjeta.value).toUpperCase().indexOf(query) >= 0) && fOficina;
          });
        } else {
          this.itemsDisplay = this.items;
        }
  }

  async setItems(data) {
    this.items = data;
    this.itemsDisplay = data;
    await this.requestUpdate();
  }

  refresh() {
    this.dispatch(this.events.refreshBandejaStock, { });
    this.getById('input-search').value = '';
    this.oficinaFilter = null;
    this.shadowRoot.querySelector('cells-ui-autocomplete-vcard').clearValue();
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-stock-vcard-shared-styles').cssText}
      ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
  }

  checkAutomatico(event) {
    let chk = event.path[0];
    let input = this.shadowRoot.getElementById(`cant-auto-input-${chk.dataset.index}`);
    if (input && chk) {
      if (event.detail.checked) {
        input.removeAttribute('disabled');
        input.classList.remove('disabled-input');
      } else {
        input.classList.add('disabled-input');
        input.value = '';
        input.setAttribute('disabled', 'disabled');
      }
    }
    //
  }

  printCheckbox(checked, index) {
    if (checked) {
      return html`<bbva-form-checkbox id = "chk-${index}" data-index = "${index}" @change = "${this.checkAutomatico}" checked ></<bbva-form-checkbox>`;
    }
    return html`<bbva-form-checkbox id = "chk-${index}" data-index = "${index}" @change = "${this.checkAutomatico}" ></<bbva-form-checkbox>`;
  }

  procesarStock(movimientos) {
    let sum = 0;
    movimientos.forEach((item) => {
      if (item.tipoMovimiento.codigo === this.ctts.keys.movOut) {
        sum = sum - item.cantidad;
      } else {
        sum = sum + item.cantidad;
      }
    });
    return sum;
  }

  getDataEdit(index) {
    const rango = this.getInputValue(`rango-input-${index}`);
    const stockMinimo = this.getInputValue(`stock-min-input-${index}`);
    const valorAutomatico = this.getInputValue(`cant-auto-input-${index}`);
    const automatico = this.getById(`chk-${index}`).checked;
    const codigoPerfilSesion = this.extract(this, 'userSession.perfil.codigo', null);
    let data = {
      stockMinimo: stockMinimo,
      automatico: automatico,
      valorAutomatico: valorAutomatico,
    };
    if(codigoPerfilSesion === this.ctts.perfiles.admin){
      data.rango = rango;
    }
    return data;
  }

  validateEdit(data, item) {
    
    if (data.stockMinimo <= 0) {
      this.showAlert({ delay: 5000, message: 'El stock mínimo no puede ser cero o negativo.' });
      return false;
    }
    if (data.rango <= 0) {
      this.showAlert({ delay: 5000, message: 'El rango no puede ser cero o negativo.' });
      return false;
    }
    if (data.automatico) {
      if (this.isEmpty(data.valorAutomatico)) {
        this.showAlert({ delay: 5000, message: 'Ingrese un valor para el pedido automático.' });
        return false;
      }
      if (parseFloat(data.valorAutomatico) <= 0) {
        this.showAlert({ delay: 5000, message: 'El valor del pedido automatico no puede ser cero o negativo.' });
        return false;
      }
      let tmpRango = data.rango 
      if(!tmpRango) {
        tmpRango = this.extract(item, 'tarjeta.rango',1);
      }
      if(parseFloat(data.valorAutomatico)%tmpRango !== 0){
        this.showAlert({ delay: 5000, message: `El valor del pedido automatico debe ser multiplo de ${tmpRango}.` });
        return false;
      }

    }
    return true;
  }

  onEdit(item, index) {
    let data = this.getDataEdit(index);
    if (this.validateEdit(data, item)) {
      this.dispatch(this.events.tarjetaOficinaEditOpenEvent, { tarjetaOficina: item, data: data });
    }
  }

  onMovimientos(item) {
    this.dispatch(this.events.stockMovimientosOpenEvent, item);
  }

  showAlert(alert) {
    let alertMessage = this.shadowRoot.querySelector('cells-ui-message-alert-vcard');
    alertMessage.add(alert);
  }

  infoAlertStock(item, index) {
    const stock = this.procesarStock(this.extract(item, 'movimientos', []));
    const stockMinimo = this.extract(item, 'stockMinimo', 0);
    if (stock < stockMinimo) {
      return 'row-danger';
    } else if ((stock - stockMinimo) < 10) {
      return 'row-warning';
    } else {
      return '';
    }
  }

  enableRango(item, index) {
    let perfil = this.extract(this, 'userSession.perfil.codigo', null);
    if(perfil === this.ctts.perfiles.admin){
      return html`<input id = "rango-input-${index}" type = "number" value = "${this.extract(item, 'tarjeta.rango', 0)}" class = "input-number" min="0" >`;
    }else{
      return html`${this.extract(item, 'tarjeta.rango', 0)}`;
    }
  }

  async onChangeOficina(event) {
    console.log('onChangeOficina', event.detail);
    this.oficinaFilter = await event.detail;
    this.filterItems(true);
    this.requestUpdate();
  }

  buildColumnOficina(type, item) {
    if(this.userSession.perfil.codigo !== this.ctts.perfiles.admin){
      return html ``;
    }
    if(type === 'head') {
      return html `<th>Oficina</th>`;
    }if(type === 'column') {
      return html `<td>${this.extract(item, 'oficina.nombre', '')}</td>`;
    }
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class = "panel-top" >
        <button @click = "${() => this.refresh()}" title = "Refrescar" class = "btn-refresh"><cells-icon size = "18" icon = "coronita:navigation" ></<cells-icon> Actualizar</button>
        <input id = "input-search" class = "input" placeholder = "Buscar" />
        <cells-ui-autocomplete-vcard
                                ?hidden = ${this.userSession.perfil.codigo !== this.ctts.perfiles.admin} 
                                name = "autocomplete-oficina-stock"
                                bgClass = "white-bg" 
                                @selected-item="${this.onChangeOficina}"
                                label="Oficina"
                                .items = ${this.oficinas}  
                                displayLabel="nombre"
                                displayValue="_id">
                  </cells-ui-autocomplete-vcard>
      </div>
      <div class = "content-table-stock">
      <table class = "green">
        <thead> 
          <tr>
            <th>Bin</th>
            <th>Descripción</th>
            <th>Formato</th>
            ${this.buildColumnOficina('head')}
            <th>Stock</th>
            <th>Rango</th>
            <th>Stock Mín.</th>
            <th>Automático</th>
            <th>Cantidad Automática</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
        ${this.itemsDisplay.map((item, index) => html`
          <tr class = "${this.infoAlertStock(item, index)}">
              <td>${this.extract(item, 'tarjeta.bin', '')}</td>
              <td>${this.extract(item, 'tarjeta.value', '')}</td>
              <td>${this.extract(item, 'tarjeta.formato', '')}</td>
              ${this.buildColumnOficina('column',item)}
              <td>${this.procesarStock(this.extract(item, 'movimientos', []))}</td>
              <td style = "text-align:center;">${this.enableRango(item, index)}</td>
              <td style = "text-align:center;"><input id = "stock-min-input-${index}" type = "number" value = "${this.extract(item, 'stockMinimo', 0)}" class = "input-number" min="0" ></td>
              <td style = "text-align:center;">${this.printCheckbox(item.automatico, index)} </td>
              <td style = "text-align:center;"><input id = "cant-auto-input-${index}" type = "number" value = "${this.extract(item, 'valorAutomatico', 0)}" class = "input-number" min="0" ></td>
              <td>
                <div class = "actions-buttons-stock" >
                  <button @click = "${() => this.onEdit(item, index)}" title = "Editar" class = "btn-action-stock"><cells-icon size = "18" icon = "coronita:edit" ></<cells-icon></button>
                  <button @click = "${() => this.onMovimientos(item)}" title = "Movimientos" class = "btn-action-stock"><cells-icon size = "18" icon = "coronita:listview" ></<cells-icon></button>
                </div>
              </td>
            </tr>
        `)}
          
       
        </tbody>
      </table>
      </div>
      ${this.itemsDisplay.length === 0 ? html` <div style = "width: 100%;
                                                        padding: 15px;
                                                        border: 1px solid #BDBDBD;
                                                        text-align: center;
                                                        color: #666;">Sin resultados que mostrar.</div>` : html``}
      <cells-ui-message-alert-vcard></cells-ui-message-alert-vcard>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiStockVcard.is, CellsUiStockVcard);
