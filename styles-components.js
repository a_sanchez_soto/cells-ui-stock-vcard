import { css, } from 'lit-element';

import { setComponentSharedStyles } from '@cells-components/cells-lit-helpers';

setComponentSharedStyles('cells-select-shared-styles', css`
.toggle{
  height:42px !important;
}
.toggle__text {
    margin-top: 12px;
}
`);

setComponentSharedStyles('bbva-button-default-shared-styles', css`
    :host {
        min-height: 42px !important;
        max-width: 100px !important;
        min-width: 50px !important;
    }
`);





export default css``;